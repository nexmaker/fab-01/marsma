### Brief introduction

* Due to the use of windows OLE technology, intuitive design technology, advanced Parasolid kernel (provided by Cambridge) and good integration technology with third-party software, SolidWorks has become the most installed and best used software in the world. 
According to the data, about 280000 SolidWorks software licenses have been issued worldwide, involving about 31000 enterprises in aerospace, locomotives, food, machinery, national defense, transportation, mold, electronic communication, medical devices, entertainment industry, daily necessities / consumer goods, discrete manufacturing, etc., which are distributed in more than 100 countries around the world. In the education market, nearly 145000 students from 4300 educational institutions around the world pass SolidWorks training courses every year.

![](https://gitlab.com/pic-01/pic-liu/uploads/12c659f3731b29ba9f5fc4ecff30e42b/20200728123120.png)
### Practive
![](https://gitlab.com/pic-01/pic-liu/uploads/2769bef3f246e6c423b8ec19c758911d/20200728105213.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/b9433be3313fa8d2bd68cabafa2ff484/20200728105219.png)
   
   #### The interface after opening the software
   ![](https://gitlab.com/pic-01/pic-liu/uploads/4971d97f399b4d23d5eb2933f447760b/20200728105227.png)
   #### Create a new part sequence
   ![](https://gitlab.com/pic-01/pic-liu/uploads/0b26b7abc8af815e76815ddf74748b89/20200728105234.png)
   #### Interface after new part
   ![](https://gitlab.com/pic-01/pic-liu/uploads/33d0b5cc09f7bf1bca9f537651961660/20200728105239.png)
   #### Sketch the selected datum plane in sequence as shown in the figure
   ![](https://gitlab.com/pic-01/pic-liu/uploads/5da8e8884c5ed2afc488b484f23a4c06/20200728105246.png)
   #### Select circle
   ![](https://gitlab.com/pic-01/pic-liu/uploads/a91b407ce99e192887db9145caffe91f/20200728105307.png)
   #### As shown in the figure, select the radius of the center of the circle, and then enter the radius value
   ![](https://gitlab.com/pic-01/pic-liu/uploads/d4c511c29eed8ee988a50d90761f66bb/20200728105312.png)
   #### Stretch as shown
   ![](https://gitlab.com/pic-01/pic-liu/uploads/280e2759cf4030233a755b56e75e2708/20200728105319.png)
   #### Select the profile and enter the thickness
   ![](https://gitlab.com/pic-01/pic-liu/uploads/dcf08a46bc6b2d62b5476808f75f6778/20200728105325.png)
   #### Name and save the part
   ![](https://gitlab.com/pic-01/pic-liu/uploads/638d76494f06275bfe763223d855a1c7/20200728105331.png)
   #### Duplicate new part
   ![](https://gitlab.com/pic-01/pic-liu/uploads/b865e314a2e43494c603328c6124d3a5/20200728105337.png)
   #### Select datum
   ![](https://gitlab.com/pic-01/pic-liu/uploads/47ebe2ccef8690cf73aab45ca30bed41/20200728105342.png)
   #### Select the rectangle command as shown in the figure
   ![](https://gitlab.com/pic-01/pic-liu/uploads/5cb0dd5e5a1c5721430c92c698176b32/20200728111421.png)
   #### Select the center of the circle and select any point
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1b8cecf7ef0e193617f9a669517befbf/20200728105350.png)
   #### Click one side to input parameters, the other side is the same
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1baa90b25cb4e72e980ccbcaa01c235e/20200728105355.png)
   #### Draw a circle in the same way
   ![](https://gitlab.com/pic-01/pic-liu/uploads/2032dc58d00aa60aecfa514f0a75e79e/20200728105441.png)
   #### Offset the circle
   ![](https://gitlab.com/pic-01/pic-liu/uploads/0eb6a5cd2d8f4c94c51c20d04f2aab05/20200728105452.png)
   #### Select the clipping command according to the operation steps, and the effect is as follows
   ![](https://gitlab.com/pic-01/pic-liu/uploads/b1a907ae279b425dd886c598e161070d/20200728105459.png)
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1deceb7f75f0b8ca549c7819ca8854e9/20200728105506.png)
   #### Remove extra lines
   ![](https://gitlab.com/pic-01/pic-liu/uploads/aa47aeed94dce8fb2e4d074515e6b1ad/20200728105824.png)
   #### Design fillet
   ![](https://gitlab.com/pic-01/pic-liu/uploads/02a329898a2ca926aa7da381360c1d65/20200728105830.png)
   #### In the same way
   ![](https://gitlab.com/pic-01/pic-liu/uploads/c20bdaca15e8447ac85e539fc83be40e/20200728105836.png)
   #### Use the offset command as shown in the figure
   ![](https://gitlab.com/pic-01/pic-liu/uploads/fe96647b40bff3135fade35316094efb/20200728105842.png)
   #### The selection circle command is set as the midpoint in the position shown in the figure, and the effect is as follows
   ![](https://gitlab.com/pic-01/pic-liu/uploads/a098ee4543c2a3d623d18eb370af6a19/20200728105848.png)
   ![](https://gitlab.com/pic-01/pic-liu/uploads/9977e5f616b9f2f46628f61dca2ecf34/20200728105906.png)
   #### Stretch, save
   ![](https://gitlab.com/pic-01/pic-liu/uploads/ce5cff66dacf1aac9c7dc04bcaf07f1d/20200728105913.png)
   #### New assembly
   ![](https://gitlab.com/pic-01/pic-liu/uploads/f227647e11c16894c833e4c4d5d6a5f2/20200728105919.png)
   #### Guide in body
   ![](https://gitlab.com/pic-01/pic-liu/uploads/6fc9e59d9050836d8603ea871f0f9b3e/20200728105923.png)
   #### In the same way, import the wheel and find that the wheel is drawn large
   ![](https://gitlab.com/pic-01/pic-liu/uploads/0ead6f047a1b3bce72d177b4b8ccbf96/20200728105949.png)
   #### Save assembly
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1964d59db02b2750b435a4c6e36f4eda/20200728105958.png)
   ![](https://gitlab.com/pic-01/pic-liu/uploads/9c17d547850859cf6c79d8aeea10b0cf/20200728110010.png)
   #### Modify the sketch dimensions according to the operation steps in the drawing
   ![](https://gitlab.com/pic-01/pic-liu/uploads/9312383b574886f3d2309fdc60f3250f/20200728110020.png)
   #### Select matching instruction
   ![](https://gitlab.com/pic-01/pic-liu/uploads/d2bab50a31c847964f2b862142c0985b/20200728110029.png)
   #### As shown in the figure, select concentric fit
   ![](https://gitlab.com/pic-01/pic-liu/uploads/ef0404b84058da07283599165394b6fe/20200728110035.png)
   #### As shown in the figure, select the plane coincidence and find the coincidence area
   ![](https://gitlab.com/pic-01/pic-liu/uploads/eb1b408797cff3219d7a82e72345a65f/20200728110041.png)
   #### Clip parts
   ![](https://gitlab.com/pic-01/pic-liu/uploads/4c2d54aca6e366e268992c344a206d92/20200728110048.png)
   #### Select one side to create a sketch
   ![](https://gitlab.com/pic-01/pic-liu/uploads/8da1f3e36023b25c10400216a3ecc0f6/20200728110054.png)
   #### Use offset command
   ![](https://gitlab.com/pic-01/pic-liu/uploads/bcc00b318714cf70545f1f728ad669c3/20200728110100.png)
   #### Use stretch cut command
   ![](https://gitlab.com/pic-01/pic-liu/uploads/b3a19e5f8f79688262049c487d3c1bd1/20200728110107.png)
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1c7e01de624d3a70d3e68ee75471eb38/20200728110115.png)
   #### In the same way, it draws four wheel pits
   ![](https://gitlab.com/pic-01/pic-liu/uploads/5d6d7a95508e8e4d20c7ecbaba4a9fd3/20200728110121.png)
   #### In the same way, the wheel is imported and matched
   #### Homework after class: notice that the front and rear wheels are missing shafts, please complete the shafts
   ![](https://gitlab.com/pic-01/pic-liu/uploads/eefbd29493a4ce807cb23f0ad1b45bc1/20200728110127.png)
   
### Reference
* SW Website：https://www.solidworks.com/zh-hans
* Functions:sketch, modeling, assembly, rendering, simulation
* Basic operation: 
  * Mouse: left button selection, middle button zoom, rotation, right-click function
  * Keyboard:ESC-cancel,enter-confirm,delete-delete
* Video：[Video collection](https://www.baidu.com/sf/vsearch?wd=solidworks&pd=video&tn=vsearch&lid=ec01a471007230f1&ie=utf-8&rsv_spt=4&rsv_bp=1&f=8&oq=solidworks&rsv_pq=ec01a471007230f1&rsv_t=d87ejTLikGsowhIuqCem5tmCPdFu3qSCzbmoZO8hlXPQUeISSkY3LaoC8NMbWw)
* Learning Forum：[SOLIDWORKS FANS](http://fans.solidworks.com.cn/portal.php?mod=list&catid=129)
* Model base： 
  * [MISUMI](https://www.misumi.com.cn/asia/incadlibrary/?utm_source=baidu&utm_medium=ppc&utm_content=ideanote-%e5%8d%8e%e4%b8%9c&utm_campaign=ideanote-%e5%8d%8e%e4%b8%9c&utm_term=sw%e6%a8%a1%e5%9e%8b)
  * [3D contentcentral](https://www.3dcontentcentral.cn/)